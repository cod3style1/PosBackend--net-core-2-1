﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public class CategoryListModel
    {
        public CategoryProductModel[] Category_List { get; set; }
        public int Category_Total { get; set; }
    }
}
