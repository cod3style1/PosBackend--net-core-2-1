﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public class CategoryProductModel
    {
        public CategoryProductModel()
        {
            Product = new HashSet<ProductModel>();
        }

        [Key]
        [Required]
        public int Id { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(5)]
        public string Id_Custom { get; set; }

        [MinLength(3)]
        [MaxLength(20)]
        [Required]
        public string Name { get; set; }

        [Required]
        [MaxLength(50)]
        public string Description { get; set; }

        public bool Status { get; set; } 

        public ICollection<ProductModel> Product { get; set; }

    }
}
