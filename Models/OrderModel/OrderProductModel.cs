﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public class OrderProductModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int Order_Count { get; set; }

        [Required]
        public int Product_Id { get; set; }
    }
}
