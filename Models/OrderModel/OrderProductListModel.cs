﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public class OrderProductListModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Order_Name { get; set; }

        [Required]
        public List<OrderProductModel> Product_List { get; set; }

        //[Required]
        public int Product_Amount { get; set; }

        public bool Status { get; set; }

        [DataType(DataType.Date)]
        public DateTime Order_Time { get; set; }

        [DataType(DataType.Date)]
        public DateTime OrderInStore_Time { get; set; }
        
        public int MemberChngeOrderId { get; set; }

        public int MemberId { get; set; }

        public MemberModel Member { get; set; }
    }
}
