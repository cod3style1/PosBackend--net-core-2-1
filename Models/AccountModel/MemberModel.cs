﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace PPos.Models
{
    public class MemberModel
    {
        public MemberModel()
        {
            Sales = new HashSet<SalesModel>();
            OrderProductList = new HashSet<OrderProductListModel>();
        }

        [Required]
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string Username { get; set; }

        [Required]
        [StringLength(50)]
        [MinLength(5)]
        public string  Fristname { get; set; }

        [Required]
        [StringLength(50)]
        [MinLength(5)]
        public string Lastname { get; set; }

        [Required]
        [MinLength(5)]
        [MaxLength(100)]
        public string Password { get; set; }

        public string Image_Url { get; set; }

        public Byte[] Image_Byte { get; set; }


        public string Image_Type { get; set; }

        public AccountRole Role { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Created { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Updated { get; set; }

        public bool Status { get; set; }

        public ICollection<SalesModel> Sales { get; set; }

        public ICollection<OrderProductListModel> OrderProductList { get; set; }
    }
}
