﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace PPos.Models
{
    public class PasswordChangeModel
    {
        [Required]
        public string New_Password { get; set; }

        [Required]
        public string Old_Password { get; set; }
    }
}
