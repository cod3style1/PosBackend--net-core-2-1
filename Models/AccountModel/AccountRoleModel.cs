﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public enum AccountRole
    {
        employee = 0,
        admin = 1
    }
}
