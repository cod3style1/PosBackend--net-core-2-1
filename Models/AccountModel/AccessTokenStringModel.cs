﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public class AccessTokenStringModel
    {
        public string AccessToken { get; set; }
    }
}
