﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public class JWTPayloadModel
    {
        public class JWTPayload
        {
            public string Username { get; set; }
            public DateTime Exp { get; set; }
        }
    }
}
