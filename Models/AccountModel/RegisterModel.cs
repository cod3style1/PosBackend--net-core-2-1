﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public class RegisterModel
    {
        [Required]
        [StringLength(20)]
        public string Username { get; set; }

        [Required]
        [StringLength(50)]
        public string Fristname { get; set; }

        [Required]
        public string Lastname { get; set; }
        [Required]
        public string Password { get; set; }

        public string Image_Url { get; set; }

        public Byte[] Image_Byte { get; set; }

        public string Image_Type { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Updated { get; set; }

    }
}
