﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace PPos.Models
{
    public class MemberProfileModel
    {
        public int Id { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string Fristname { get; set; }

        [Required]
        public string Lastname { get; set; }

        public string Image_Url { get; set; }

        [JsonIgnore]
        public Byte[] Image_Byte { get; set; }

        [JsonIgnore]
        public string Image_Type { get; set; }

        public string Image { get; set; }

        public AccountRole Role { get; set; }

        public DateTime Created { get; set; }

        public DateTime Updated { get; set; }

        public bool Status { get; set; }
    }
}
