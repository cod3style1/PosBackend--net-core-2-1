﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public class MemberListModel
    {
        public MemberProfileModel[] Members { get; set; }
        public int Total_Member { get; set; } 
    }
}
