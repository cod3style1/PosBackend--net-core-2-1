﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public class ReportSalesListModel
    {
        public ReportSalesModel[] ReportSales_List { get; set; }

        public int Report_Count { get; set; }
    }
}
