﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public class ReportBestSalerList
    {
        public ReportBestSalerModel[] BestSaler_List { get; set; }
        public int Total_BestSaler { get; set; }
    }
}
