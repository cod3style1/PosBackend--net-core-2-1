﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public class CostAndProfitModel
    {
        public int Cost { get; set; }
        public int Profit { get; set; }
        public int Total_Sales { get; set; }
        public DateTime Sales_Time { get; set; }
    }
}
