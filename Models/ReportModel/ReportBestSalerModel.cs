﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public class ReportBestSalerModel
    {
        public string Product_Name { get; set; }
        public int Product_TotalSales { get; set; }
        public int ProductId { get; set; }
    }
}
