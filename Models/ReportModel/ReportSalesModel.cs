﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public class ReportSalesModel
    {
        public int Total_Price { get; set; }
        public DateTime Sales_Time { get; set; }
    }
}
