﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models.ReportModel
{
    public class ReportProductCompareModel
    {
        public ReportSalesProductModel[] SalesProduct_First { get; set; }

        public ReportSalesProductModel[] SalesProduct_Second { get; set; }
    }
}
