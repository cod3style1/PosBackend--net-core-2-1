﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public class ReportSalesProductModel
    {
        public int Sales_Count { get; set; }
        public DateTime Sales_Time { get; set; }
        public int SalesProduct_Id { get; set; }
    }
}
