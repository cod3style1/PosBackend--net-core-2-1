﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models.ReportModel
{
    public class CostAndProfitList
    {
        public CostAndProfitModel[] CostAndProfit_List { get; set; }
        public int Total_CostAndProfit  { get; set; }
    }
}
