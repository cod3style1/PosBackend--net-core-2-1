﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public class ReportSalesProductListModel
    {
        public ReportSalesProductModel[] SalesProduct_List { get; set; }
        public int SalesProduct_Total { get; set; }
    }
}
