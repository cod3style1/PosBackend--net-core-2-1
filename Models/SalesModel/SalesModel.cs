﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public class SalesModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MinLength(1)]
        [MaxLength(20)]
        public List<SalesOrderModel> Sales_List { get; set; }

        public int Total_Price { get; set; }

        [Required]
        public int Payment { get; set; }

        public bool Status { get; set; }

        [DataType(DataType.Date)]
        public DateTime Sales_Time { get; set; }

        [DataType(DataType.Date)]
        public DateTime CancelSales_Time { get; set; }

        public string MemberCancelBill { get; set; }
         
        public int CostProductBill { get; set; }

        public int MemberId { get; set; }

        public MemberModel Member { get; set; }
    }
}
