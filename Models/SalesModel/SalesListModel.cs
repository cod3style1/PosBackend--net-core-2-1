﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public class SalesListModel
    {
        public SalesModel[] Sales_List { get; set; }
        public int Sales_Total { get; set; }
    }
}
