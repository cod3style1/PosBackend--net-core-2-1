﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public class SalesOrderModel
    {
        [Key]
        [Required]
        public int Id { get; set; }

        [Required]
        public int Sales_Count { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Sales_Time { get; set; }

        public int ProductId { get; set; }

        public ProductModel Product { get; set; }
    }
}
