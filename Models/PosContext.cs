﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public class PosContext: DbContext
    {

        public PosContext(DbContextOptions<PosContext> options) : base(options)
        {
        }

        public DbSet<MemberModel> Members { get; set; }

        public DbSet<ProductModel> Products { get; set; }

        public DbSet<SalesModel> Sales { get; set; }

        public DbSet<SalesOrderModel> SalesOrder { get; set; }

        public DbSet<OrderProductModel> Orders { get; set; }

        public DbSet<OrderProductListModel> OrderProducts { get; set; }
    }
}
