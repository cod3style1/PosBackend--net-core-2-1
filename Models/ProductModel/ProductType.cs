﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public enum ProductType
    {
        อัน = 1,
        ขวด = 2,
        ชิ้น = 3,
        ด้าม = 4,
        เล่ม = 5,
        ห่อ = 6,
        เครื่อง = 7,
        แท่ง = 8,
        ลูก = 9,
        แผ่น = 10,
        เส้น = 11,
    }
}
