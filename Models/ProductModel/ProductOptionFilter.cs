﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public class ProductOptionFilter
    {
        [Required]
        public int Start_Page { get; set; }

        [Required]
        public int Limit_Page { get; set; }

        public string Search_Text { get; set; }

        public string Search_Type { get; set; }

        public string Search_DefaultText { get; set; }

        public string Search_DefaultType { get; set; }
    }
}
