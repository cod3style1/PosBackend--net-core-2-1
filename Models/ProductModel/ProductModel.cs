﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace PPos.Models
{
    public class ProductModel 
    {
        public ProductModel()
        {
            SalesOrders = new HashSet<SalesOrderModel>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(13)]
        [MinLength(13)]
        public string Barcode { get; set; }

        [MinLength(3)]
        [MaxLength(5)]
        public string Barcode_Custom { get; set; }
        
        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string Name { get; set; }

        [MaxLength(100)]
        public string Description { get; set; }

        public string Image_Url { get; set; }

        public Byte[] Image_Byte { get; set; }

        public string Image_Type { get; set; }

        [Required]
        public DateTime Expired { get; set; }

        [Required]
        public decimal Cost_Product { get; set; }

        [Required]
        public int Price { get; set; }

        [Required]
        public int Amount_Product { get; set; }

        public bool Status { get; set; }

        public ProductType Type { get; set; }

        public ProductCategory ProductCategoryId { get; set; }

        public ICollection<SalesOrderModel> SalesOrders { get; set; }
    }
}
