﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Models
{
    public class ProductListModel
    {
        public ProductModel[] Product_List { get; set; }
        public int Product_Total { get; set; }
    }
}
