﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PPos.Migrations
{
    public partial class updatesalesmodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CostProductBill",
                table: "Sales",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CostProductBill",
                table: "Sales");
        }
    }
}
