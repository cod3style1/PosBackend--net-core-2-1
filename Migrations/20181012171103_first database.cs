﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PPos.Migrations
{
    public partial class firstdatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Members",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Username = table.Column<string>(maxLength: 20, nullable: false),
                    Fristname = table.Column<string>(maxLength: 50, nullable: false),
                    Lastname = table.Column<string>(maxLength: 50, nullable: false),
                    Password = table.Column<string>(maxLength: 100, nullable: false),
                    Image_Url = table.Column<string>(nullable: true),
                    Image_Byte = table.Column<byte[]>(nullable: true),
                    Image_Type = table.Column<string>(nullable: true),
                    Role = table.Column<int>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Members", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Barcode = table.Column<string>(maxLength: 13, nullable: false),
                    Barcode_Custom = table.Column<string>(maxLength: 5, nullable: true),
                    Name = table.Column<string>(maxLength: 30, nullable: false),
                    Description = table.Column<string>(maxLength: 50, nullable: true),
                    Image_Url = table.Column<string>(nullable: true),
                    Image_Byte = table.Column<byte[]>(nullable: true),
                    Image_Type = table.Column<string>(nullable: true),
                    Expired = table.Column<DateTime>(nullable: false),
                    Cost_Product = table.Column<decimal>(nullable: false),
                    Price = table.Column<int>(nullable: false),
                    Amount_Product = table.Column<int>(nullable: false),
                    Status = table.Column<bool>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    ProductCategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrderProducts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Order_Name = table.Column<string>(maxLength: 50, nullable: false),
                    Product_Amount = table.Column<int>(nullable: false),
                    Status = table.Column<bool>(nullable: false),
                    Order_Time = table.Column<DateTime>(nullable: false),
                    OrderInStore_Time = table.Column<DateTime>(nullable: false),
                    MemberChngeOrderId = table.Column<int>(nullable: false),
                    MemberId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderProducts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderProducts_Members_MemberId",
                        column: x => x.MemberId,
                        principalTable: "Members",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Sales",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Total_Price = table.Column<int>(nullable: false),
                    Payment = table.Column<int>(nullable: false),
                    Status = table.Column<bool>(nullable: false),
                    Sales_Time = table.Column<DateTime>(nullable: false),
                    CancelSales_Time = table.Column<DateTime>(nullable: false),
                    MemberCancelBill = table.Column<string>(nullable: true),
                    MemberId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sales", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sales_Members_MemberId",
                        column: x => x.MemberId,
                        principalTable: "Members",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Order_Count = table.Column<int>(nullable: false),
                    Product_Id = table.Column<int>(nullable: false),
                    OrderProductListModelId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_OrderProducts_OrderProductListModelId",
                        column: x => x.OrderProductListModelId,
                        principalTable: "OrderProducts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SalesOrder",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Sales_Count = table.Column<int>(nullable: false),
                    Sales_Time = table.Column<DateTime>(nullable: false),
                    ProductId = table.Column<int>(nullable: false),
                    SalesModelId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SalesOrder", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SalesOrder_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SalesOrder_Sales_SalesModelId",
                        column: x => x.SalesModelId,
                        principalTable: "Sales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OrderProducts_MemberId",
                table: "OrderProducts",
                column: "MemberId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_OrderProductListModelId",
                table: "Orders",
                column: "OrderProductListModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Sales_MemberId",
                table: "Sales",
                column: "MemberId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesOrder_ProductId",
                table: "SalesOrder",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesOrder_SalesModelId",
                table: "SalesOrder",
                column: "SalesModelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "SalesOrder");

            migrationBuilder.DropTable(
                name: "OrderProducts");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Sales");

            migrationBuilder.DropTable(
                name: "Members");
        }
    }
}
