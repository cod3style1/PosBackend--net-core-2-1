﻿using PPos.Interfaces;
using PPos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Services
{
    public class OrderProductService : IOrderProductService
    {
        private PosContext db;
        public OrderProductService(PosContext _context)
        {
            db = _context;
        }
        
        public void AdjustOrderProductById(int id, string username)
        {
            try
            {
                var order = this.GetByOrderId(id);
                int userId = this.db.Members.FirstOrDefault(it => it.Username.Equals(username)).Id;

                if (order == null) throw new Exception("not found order id");
                if (order.Status == true) throw new Exception("Can not add products to the system. Due to duplicate orders");
                foreach (var item in order.Product_List)
                {
                    var product = this.db.Products.FirstOrDefault(it => it.Id.Equals(item.Product_Id));

                    if (product == null) throw new Exception("ไม่พบสินค้าตามรหัส");

                    product.Amount_Product = (product.Amount_Product + item.Order_Count);

                    this.db.Products.Update(product);
                    this.db.SaveChanges();
                };

                order.MemberId = userId;
                order.Status = true;
                order.OrderInStore_Time = DateTime.Now;

                this.db.OrderProducts.Update(order);
                this.db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }
        
        public void CancelOrderProductById(int id)
        {
            try
            {
                var order = this.GetByOrderId(id);

                if (order == null) throw new Exception("not found order id");

                order.Status = false;

                this.db.OrderProducts.Update(order);
                this.db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }
        
        public OrderProductListModel GetOrderProductById(int id)
        {
            try
            {
                var order = this.GetByOrderId(id);
                if (order == null) throw new Exception("not found order id");
                return order;
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }

        public OrderProductListModel[] GetOrderProducts(OrderOptionFilter filter)
        {
            try
            {
                var orders = this.db.OrderProducts.Select(it => new OrderProductListModel
                {
                    Id = it.Id,
                    Order_Name = it.Order_Name,
                    Product_List = it.Product_List,
                    OrderInStore_Time = it.OrderInStore_Time,
                    Order_Time = it.Order_Time,
                    Product_Amount = it.Product_List.Count,
                    MemberId = it.MemberId,
                    Status = it.Status
                }).ToArray();

                orders = this.OrderOptionFilter(orders, filter);

                return orders;
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }

        private OrderProductListModel[] OrderOptionFilter(OrderProductListModel[] orders, OrderOptionFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.Search_Text) && !string.IsNullOrEmpty(filter.Search_Type))
            {
               IEnumerable<OrderProductListModel> Orders = new OrderProductListModel[] { };

                string SearchText = filter.Search_Text.ToUpper();
                string SearchType = filter.Search_Type;

                switch (SearchType)
                {
                    case "Expired":
                        break;
                    default:
                        Orders = from it in orders
                                   where it.GetType().GetProperty(filter.Search_Type)
                                         .GetValue(it).ToString().ToUpper()
                                         .Contains(SearchText)
                                   select it;
                        break;
                }
                orders = Orders.ToArray();
            }
            return orders.Skip((filter.Start_Page - 1) * filter.Limit_Page)
                                 .Take(filter.Limit_Page).ToArray();
        }

        private OrderProductListModel GetByOrderId(int id)
        {
            var order = this.db.OrderProducts.Select(it => new OrderProductListModel
            {
                Id = it.Id,
                Order_Name = it.Order_Name,
                Product_List = it.Product_List,
                OrderInStore_Time = it.OrderInStore_Time,
                Order_Time = it.Order_Time,
                Product_Amount = it.Product_List.Count,
                MemberId = it.MemberId,
                Status = it.Status,
                Member = it.Member
            }).FirstOrDefault(it => it.Id.Equals(id));

            if (order == null) throw new Exception("not found order id");

            return order;
        }

        public void InsertOrderProduct(OrderProductListModel model, string username)
        {
            try
            {
                int userId = this.db.Members.FirstOrDefault(it => it.Username.Equals(username)).Id;

                OrderProductListModel order = new OrderProductListModel
                {
                    Order_Name = model.Order_Name,
                    Product_List = model.Product_List,
                    Order_Time = DateTime.Now,
                    Product_Amount = model.Product_List.Count,
                    MemberId = userId,
                    Status = false
                };

                this.db.OrderProducts.Add(order);
                this.db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }


        public void UpdateOrderById(OrderProductListModel model, int id, string username)
        {
            try
            {
                var order = this.GetByOrderId(id);

                if (order.Status == true) throw new Exception("this order actived cannot update orders");

                foreach (var item in order.Product_List)
                {
                    this.DeleteProductOder(item.Id);
                }

                order.Order_Name = model.Order_Name;
                order.Product_List = model.Product_List;
                order.Product_Amount = model.Product_List.Count;
                order.MemberChngeOrderId = model.MemberId; //รายชื่อผู้อับเดต
                order.Order_Time = DateTime.Now;

                this.db.OrderProducts.Update(order);
                this.db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }

        private void DeleteProductOder(int orderId)
        {
            OrderProductModel orderDeleted = this.db.Orders.FirstOrDefault(it => it.Id.Equals(orderId));

            if (orderDeleted == null) throw new Exception("not found order id for deleted");

            this.db.Orders.Remove(orderDeleted);
            this.db.SaveChanges();
        }
    }
}
