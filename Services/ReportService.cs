﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using PPos.Models;
using PPos.Models.ReportModel;

namespace PPos.Services
{
    public class ReportService : IReportService
    {
        private PosContext db;

        public ReportService(PosContext _context)
        {
            db = _context;
        }
        // ยอดขายสินค้า
        public ReportSalesProductListModel GetProductSalesReportById(ReportOptionFilter filter, int _id)
        {
            try
            {
                ReportSalesProductModel[] salesProductReport = new ReportSalesProductModel[] { };
                //SalesOrderModel[] salesOrdersList = new SalesOrderModel[] { };
                //IEnumerable<SalesModel> checkSalesStatus = new SalesModel[] { };

                //checkSalesStatus = from it in this.db.Sales
                //                   where it.Status == true
                //                   select it;

                //salesOrdersList = checkSalesStatus.Select(it )


                var salesProducts = this.db.SalesOrder.Select(it => new ReportSalesProductModel
                {
                    Sales_Count = it.Sales_Count,
                    Sales_Time = it.Sales_Time,
                    SalesProduct_Id = it.ProductId
                }).ToArray();

                if (salesProducts == null) throw new Exception("not found sales product");

                salesProductReport = this.SearchSalesProductId(salesProducts, _id);

                salesProductReport = this.sumTotalSalesProductReport(salesProductReport);

                salesProducts = this.SalesProductReportOptionFilter(salesProductReport, filter.Search_DefaultText, filter.Search_DefaultType);

                salesProducts = this.SalesProductReportOptionFilter(salesProductReport, filter.Search_Text, filter.Search_Type);

                ReportSalesProductListModel salesProductList = new ReportSalesProductListModel
                {
                    
                    SalesProduct_List = salesProducts,
                    SalesProduct_Total = salesProducts.Length,
                };

                return salesProductList;
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }
        
        private ReportSalesProductModel[] SearchSalesProductId(ReportSalesProductModel[] salesProducts, int _id)
        {
            var SalesList = from it in salesProducts
                            where it.SalesProduct_Id.Equals(_id)
                            select it;

            if (SalesList == null) throw new Exception("not found sales product id");

            salesProducts = SalesList.OrderBy(it => it.Sales_Time).ToArray();

            return salesProducts;
        }

        private ReportSalesProductModel[] sumTotalSalesProductReport(ReportSalesProductModel[] salesProductsreports)
        {
            List<ReportSalesProductModel> sumCountSalesProduct = new List<ReportSalesProductModel>();
            if (salesProductsreports != null)
            {
                foreach (var item in salesProductsreports)
                {
                    var salesProductsreport = sumCountSalesProduct.SingleOrDefault(it =>
                    {
                        return it.Sales_Time.Date.Equals(item.Sales_Time.Date);
                    });

                    if (salesProductsreport != null)
                    {
                        salesProductsreport.Sales_Count += item.Sales_Count;
                    }
                    else
                    {
                        sumCountSalesProduct.Add(item);
                    }
                }
                salesProductsreports = sumCountSalesProduct.ToArray();
            }
            return salesProductsreports;
        }

        private ReportSalesProductModel[] SalesProductReportOptionFilter(ReportSalesProductModel[] salesProducts, string searchText, string searchType)
        {
            if (!string.IsNullOrEmpty(searchText) && !string.IsNullOrEmpty(searchType))
            {
                IEnumerable<ReportSalesProductModel> Report = new ReportSalesProductModel[] { };

                string SearchText = searchText.ToUpper();

                switch (searchType)
                {
                    case "SalesNow": // Default search ค้นหาใบเสร็จของเดือนปัจจุบัน
                        DateTime date = DateTime.Now;
                        Report = from it in salesProducts
                                 where it.Sales_Time.Month == date.Month && it.Sales_Time.Year == date.Year
                                 select it;
                        break;
                    case "SalesTime":
                        var dateSearch = searchText.Split("&");
                        DateTime toDate = DateTime.Parse(dateSearch[0]);
                        DateTime fromDate = DateTime.Parse(dateSearch[1]);

                        Report = from it in salesProducts
                                 where it.Sales_Time.Date <= fromDate.Date && it.Sales_Time.Date >= toDate.Date
                                 select it;
                        break;
                    default:
                        Report = from it in salesProducts
                                 where it.GetType().GetProperty(searchType)
                                       .GetValue(it).ToString().ToUpper()
                                       .Contains(SearchText)
                                 select it;
                        break;
                }
                salesProducts = Report.ToArray();
            }
            return salesProducts;
        }

        // ยอดขายทั้งหมด
        public ReportSalesListModel GetSalesReport(ReportOptionFilter filter)
        {
            try
            {
                ReportSalesModel[] reportSalesList = new ReportSalesModel[] { };

                var reports = this.db.Sales.Select(it => new ReportSalesModel
                {
                    Total_Price = it.Total_Price,
                    Sales_Time = it.Sales_Time
                }).OrderBy(it => it.Sales_Time).ToArray();

                reportSalesList = this.sumTotalPriceSalesReport(reports);

                reportSalesList = this.ReportOptionFilter(reportSalesList, filter.Search_DefaultText, filter.Search_DefaultType);

                reportSalesList = this.ReportOptionFilter(reportSalesList, filter.Search_Text, filter.Search_Type);

                ReportSalesListModel repostList = new ReportSalesListModel
                {
                    ReportSales_List = reportSalesList,
                    Report_Count = reportSalesList.Length
                };

                return repostList;
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }

        private ReportSalesModel[] ReportOptionFilter(ReportSalesModel[] reports, string searchText, string searchType)
        {
            if (!string.IsNullOrEmpty(searchText) && !string.IsNullOrEmpty(searchType))
            {
                IEnumerable<ReportSalesModel> Report = new ReportSalesModel[] { };

                string SearchText = searchText.ToUpper();

                switch (searchType)
                {
                    case "SalesNow": // Default search ค้นหาใบเสร็จของเดือนปัจจุบัน
                        DateTime date = DateTime.Now;
                        Report = from it in reports
                                where it.Sales_Time.Month == date.Month && it.Sales_Time.Year == date.Year
                                select it;
                        break;
                    case "SalesTime":
                        var dateSearch = searchText.Split("&");
                        DateTime toDate = DateTime.Parse(dateSearch[0]);
                        DateTime fromDate = DateTime.Parse(dateSearch[1]);

                        Report = from it in reports
                                where it.Sales_Time.Date <= fromDate.Date && it.Sales_Time.Date >= toDate.Date
                                select it;
                        break;
                    default:
                        Report = from it in reports
                                where it.GetType().GetProperty(searchType)
                                      .GetValue(it).ToString().ToUpper()
                                      .Contains(SearchText)
                                select it;
                        break;
                }
                reports = Report.ToArray();
            }
            return reports;
        }

        private ReportSalesModel[] sumTotalPriceSalesReport(ReportSalesModel[] reports)
        {
            List<ReportSalesModel> sumTotalPriceSales = new List<ReportSalesModel>();
            if (reports != null)
            {
                foreach (var item in reports)
                {
                    var report = sumTotalPriceSales.SingleOrDefault(it =>                    
                    {
                        return it.Sales_Time.Date.Equals(item.Sales_Time.Date);
                    });

                    if (report != null)
                    {
                        report.Total_Price += item.Total_Price;
                    }
                    else
                    {
                        sumTotalPriceSales.Add(item);
                    }
                }
                reports = sumTotalPriceSales.ToArray();
            }
            return reports;
        }

        public ReportProductCompareModel GetCompareProductSalesReport(ReportOptionFilter filter)
        {
            try
            {
                if (string.IsNullOrEmpty(filter.Search_FirstProdoctBarcode) && string.IsNullOrEmpty(filter.Search_SecondProdoctBarcode)) throw new Exception("plesse enter barode product to compare");

                ReportSalesProductModel[] first_salesProductReport = new ReportSalesProductModel[] { };

                ReportSalesProductModel[] secound_salesProductReport = new ReportSalesProductModel[] { };

                ReportProductCompareModel reportProductCompare = new ReportProductCompareModel { };

                var salesProducts = this.db.SalesOrder.Include(it => it.Product.Id).Select(it => new ReportSalesProductModel
                {
                    Sales_Count = it.Sales_Count,
                    Sales_Time = it.Sales_Time,
                    SalesProduct_Id = it.ProductId
                }).ToArray();


                var fistProductId = this.db.Products.FirstOrDefault(it => it.Barcode.Equals(filter.Search_FirstProdoctBarcode)).Id;
                var secoundProductId = this.db.Products.FirstOrDefault(it => it.Barcode.Equals(filter.Search_SecondProdoctBarcode)).Id;

                first_salesProductReport = this.SearchSalesProductId(salesProducts, fistProductId);

                first_salesProductReport = this.sumTotalSalesProductReport(first_salesProductReport);

                first_salesProductReport = this.SalesProductReportOptionFilter(first_salesProductReport, filter.Search_DefaultText, filter.Search_DefaultType);

                first_salesProductReport = this.SalesProductReportOptionFilter(first_salesProductReport, filter.Search_Text, filter.Search_Type);


                secound_salesProductReport = this.SearchSalesProductId(salesProducts, secoundProductId);

                secound_salesProductReport = this.sumTotalSalesProductReport(secound_salesProductReport);

                secound_salesProductReport = this.SalesProductReportOptionFilter(secound_salesProductReport, filter.Search_DefaultText, filter.Search_DefaultType);

                secound_salesProductReport = this.SalesProductReportOptionFilter(secound_salesProductReport, filter.Search_Text, filter.Search_Type);


                reportProductCompare.SalesProduct_First = first_salesProductReport;
                reportProductCompare.SalesProduct_Second = secound_salesProductReport;

                return reportProductCompare;

            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }

        
        // ค้นหาสินค้าขายดี
        public ReportBestSalerList GetProductBestSales(ReportOptionFilter filter)
        {
            try
            {
                ReportBestSalerModel[] productsSalesFilter= new ReportBestSalerModel[] { };
                SalesOrderModel[] salesOrders = new SalesOrderModel[] { };

                salesOrders = this.db.SalesOrder.Include(it => it.Product.Id).Select(it => new SalesOrderModel
                {
                    Sales_Time = it.Sales_Time,
                    Sales_Count = it.Sales_Count,
                    ProductId = it.ProductId
                }).ToArray();

                // ค้นหาด้วยเงื่อนไขอื่นๆ 
                salesOrders = this.BestSalesOptionFilter(salesOrders, filter.Search_DefaultText, filter.Search_DefaultType);

                salesOrders = this.BestSalesOptionFilter(salesOrders, filter.Search_Text, filter.Search_Type);

                productsSalesFilter = salesOrders.Select(it => new ReportBestSalerModel
                {
                    Product_Name = this.db.Products.SingleOrDefault(pd => pd.Id.Equals(it.ProductId)).Name,
                    ProductId = it.ProductId,
                    Product_TotalSales = it.Sales_Count,
                }).ToArray();

                productsSalesFilter = this.sumTotalSalesProductById(productsSalesFilter).OrderByDescending(it => it.Product_TotalSales).ToArray();

                ReportBestSalerList productBestsalesList = new ReportBestSalerList
                {
                    BestSaler_List = productsSalesFilter.Skip((filter.Start_Page - 1) * filter.Limit_Page)
                   .Take(filter.Limit_Page).ToArray(),
                    Total_BestSaler = productsSalesFilter.Count()
                };

                return productBestsalesList;
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }

        private ReportBestSalerModel[] sumTotalSalesProductById(ReportBestSalerModel[] productsSales)
        {
            List<ReportBestSalerModel> sumTotalProductsSales = new List<ReportBestSalerModel>();
            if (productsSales != null)
            {
                foreach (var item in productsSales)
                {
                    var salesProductsreport = sumTotalProductsSales.SingleOrDefault(it =>
                    {
                        return it.ProductId.Equals(item.ProductId);
                    });

                    if (salesProductsreport != null)
                    {
                        salesProductsreport.Product_TotalSales += item.Product_TotalSales;
                    }
                    else
                    {
                        sumTotalProductsSales.Add(item);
                    }
                }
                productsSales = sumTotalProductsSales.ToArray();
            }
            return productsSales;
        }

        private SalesOrderModel[] BestSalesOptionFilter(SalesOrderModel[] salesOrders, string searchText, string searchType)
        {
            if (!string.IsNullOrEmpty(searchText) && !string.IsNullOrEmpty(searchType))
            {
                IEnumerable<SalesOrderModel> orders = new SalesOrderModel[] { };

                string SearchText = searchText.ToUpper();

                switch (searchType)
                {
                    case "SalesNow": // Default search ค้นหาใบเสร็จของเดือนปัจจุบัน
                        DateTime date = DateTime.Now;
                        orders = from it in salesOrders
                                 where it.Sales_Time.Month.Equals(date.Month)  && it.Sales_Time.Year.Equals(date.Year)
                                 select it;
                        break;
                    case "SalesTime":
                        var dateSearch = searchText.Split("&");
                        DateTime toDate = DateTime.Parse(dateSearch[0]);
                        DateTime fromDate = DateTime.Parse(dateSearch[1]);
                        orders = from it in salesOrders
                                 where it.Sales_Time.Date <= fromDate.Date && it.Sales_Time.Date >= toDate.Date
                                 select it;
                        break;
                    default:
                        orders = from it in salesOrders
                                 where it.GetType().GetProperty(searchType)
                                       .GetValue(it).ToString().ToUpper()
                                       .Contains(SearchText)
                                 select it;
                        break;
                }
                salesOrders = orders.ToArray();
            }
            return salesOrders;
        }


        public CostAndProfitList GetCostAndProfit(ReportOptionFilter filter)
        {
            try
            {

                CostAndProfitModel[] costAndProfits = new CostAndProfitModel[] { };

                costAndProfits = this.db.Sales.Select(it => new CostAndProfitModel
                {
                    Cost = it.CostProductBill,
                    Profit = (it.Total_Price - it.CostProductBill),
                    Sales_Time = it.Sales_Time,
                    Total_Sales = it.Total_Price
                }).ToArray();

                costAndProfits = this.sumTotalCostAndProfit(costAndProfits);

                costAndProfits = this.CostAndProfitOptionFilter(costAndProfits, filter.Search_DefaultText, filter.Search_DefaultType);

                CostAndProfitList costAndProfitList = new CostAndProfitList
                {
                    CostAndProfit_List = costAndProfits,
                    Total_CostAndProfit = costAndProfits.Count()
                };

                return costAndProfitList;
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            } 
        }

        private CostAndProfitModel[] sumTotalCostAndProfit(CostAndProfitModel[] costAndProfits)
        {
            List<CostAndProfitModel> sumTotalCostAndProfit = new List<CostAndProfitModel>();
            if (costAndProfits != null)
            {
                foreach (var item in costAndProfits)
                {
                    var costAndProfitTime = sumTotalCostAndProfit.SingleOrDefault(it =>
                    {
                        return it.Sales_Time.Date.Equals(item.Sales_Time.Date);
                    });

                    if (costAndProfitTime != null)
                    {
                        costAndProfitTime.Cost += item.Cost;
                        costAndProfitTime.Profit += item.Profit;
                        costAndProfitTime.Total_Sales += item.Total_Sales;
                    }
                    else
                    {
                        sumTotalCostAndProfit.Add(item);
                    }
                }
                costAndProfits = sumTotalCostAndProfit.ToArray();
            }
            return costAndProfits;
        }

        private CostAndProfitModel[] CostAndProfitOptionFilter(CostAndProfitModel[] costAndProfits, string searchText, string searchType)
        {
            if (!string.IsNullOrEmpty(searchText) && !string.IsNullOrEmpty(searchType))
            {
                IEnumerable<CostAndProfitModel> costAndProfitList = new CostAndProfitModel[] { };

                string SearchText = searchText.ToUpper();

                switch (searchType)
                {
                    case "SalesDay": 
                         DateTime dateNow = DateTime.Now;
                        costAndProfitList = from it in costAndProfits
                                            where it.Sales_Time.Date.Equals(dateNow.Date)
                                            select it;
                        break;
                    case "SalesNow": 
                        DateTime date = DateTime.Now;
                        costAndProfitList = from it in costAndProfits
                                 where it.Sales_Time.Month.Equals(date.Month) && it.Sales_Time.Year.Equals(date.Year)
                                 select it;
                        break;
                    case "SalesTime":
                        var dateSearch = searchText.Split("&");
                        DateTime toDate = DateTime.Parse(dateSearch[0]);
                        DateTime fromDate = DateTime.Parse(dateSearch[1]);
                        costAndProfitList = from it in costAndProfits
                                 where it.Sales_Time.Date <= fromDate.Date && it.Sales_Time.Date >= toDate.Date
                                 select it;
                        break;
                    default:
                        costAndProfitList = from it in costAndProfits
                                 where it.GetType().GetProperty(searchType)
                                       .GetValue(it).ToString().ToUpper()
                                       .Contains(SearchText)
                                 select it;
                        break;
                }
                costAndProfits = costAndProfitList.ToArray();
            }
            return costAndProfits;
        }
    }
}
