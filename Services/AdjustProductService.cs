﻿using PPos.Interfaces;
using PPos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Services
{
    public class AdjustProductService : IAdjustProductService
    {
        private PosContext db;
        private IProductService productService;

        public AdjustProductService(PosContext _context)
        {
            db = _context;
            productService = new ProductService(db);
        }

        public void AdjustStockProduct(SalesModel model)
        {
            bool canserBill = false;
            try
            {
                foreach (SalesOrderModel item in model.Sales_List)
                {
                    this.productService.AdjustProductById(item.Sales_Count, item.ProductId, canserBill);
                }
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }

        public void CancelBill(SalesModel model)
        {
            bool canserBill = true;
            try
            {
                foreach (SalesOrderModel item in model.Sales_List)
                {
                    this.productService.AdjustProductById(item.Sales_Count, item.ProductId, canserBill);
                }
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }
    }
}
