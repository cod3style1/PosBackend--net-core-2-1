﻿using Jose;
using PPos.Interfaces;
using PPos.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PPos.Models.JWTPayloadModel;

namespace PPos.Services
{
    public class JwtAccessTokenService : IAccessTokenServices
    { 
        private PosContext db;

        private byte[] serectkey = Encoding.UTF8.GetBytes("Codestyle");

        public JwtAccessTokenService(PosContext _context)
        {
            db = _context;
        }

        public string GenerateAccessToken(string _username, int minute = 60)
        {            
            JWTPayload payload = new JWTPayload
            {
                Username = _username,
                Exp = DateTime.UtcNow.AddMinutes(minute)
            };
            return JWT.Encode(payload, serectkey, JwsAlgorithm.HS256);
        }

        public MemberModel VerrifyAccessToken(string accessToken)
        {
            try
            {
                JWTPayload Payload = JWT.Decode<JWTPayload>(accessToken, serectkey);

                if (Payload == null) return null;
                if (Payload.Exp < DateTime.UtcNow) return null;

                return db.Members.SingleOrDefault(it => it.Username.Equals(Payload.Username));
            }
            catch
            {
                throw null;
            }
        }
    }
}
