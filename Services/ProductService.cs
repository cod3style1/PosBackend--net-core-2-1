﻿using PPos.Interfaces;
using PPos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace PPos.Services
{
    public class ProductService : IProductService
    {
        private PosContext db;
        public ProductService(PosContext _context)
        {
            db = _context;
        }

        public void AddProduct(ProductModel model)
        {
            try
            {
                ProductModel product = new ProductModel
                {
                    Id = model.Id,
                    Name = model.Name,
                    Barcode = model.Barcode,
                    Barcode_Custom = model.Barcode_Custom,
                    Description = model.Description,
                    Expired = model.Expired,
                    Amount_Product = model.Amount_Product,
                    Cost_Product = model.Cost_Product,   
                    Price = model.Price,
                    Status = true,
                    ProductCategoryId = model.ProductCategoryId,
                    Type = model.Type,
                };

                this.onCoverBase64toImage(product, model.Image_Url);
                this.db.Products.Add(model);
                this.db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }

        public ProductModel GetProductById(int id)
        {
            try
            {
                var product = this.db.Products.FirstOrDefault(it => it.Id.Equals(id));

                if (product == null) throw new Exception("ไม่พบสินค้าตามรหัส");

                return product;
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }

        public ProductListModel GetProducts(ProductOptionFilter filter)
        {
            try
            {
                ProductModel[] ProductFilter = new ProductModel[] { };

                // การชนิดค้นหาเบื่องต้นไม่ควรเท่ากับ การค้นหาธรรมดา
                if (!string.IsNullOrEmpty(filter.Search_DefaultText) && !string.IsNullOrEmpty(filter.Search_Text) && filter.Search_Type == filter.Search_DefaultType) throw new Exception("การค้นหาเบื้องต้นซ้ำกัน");

                var products = this.db.Products.Select(it => new ProductModel
                {
                    Id = it.Id,
                    Barcode = it.Barcode,
                    Barcode_Custom = it.Barcode_Custom,
                    Name = it.Name,
                    Description = it.Description,
                    Price = it.Price,
                    Expired = it.Expired,
                    ProductCategoryId = it.ProductCategoryId,
                    Cost_Product = it.Cost_Product,
                    Amount_Product = it.Amount_Product,
                    Type = it.Type,
                    Image_Url = it.Image_Url,
                    Status = it.Status
                }).ToArray();

                if (products == null) throw new Exception("ไม่พบสินค้า");

                // ค้นหาแบบ Default
                ProductFilter = this.ProductOptionFilter(products, filter.Search_DefaultText, filter.Search_DefaultType);

                // ค้นหาแบบ ธรรมดา
                ProductFilter = this.ProductOptionFilter(ProductFilter, filter.Search_Text, filter.Search_Type);


                if (ProductFilter.Length == 0) throw new Exception("ไม่พบสินค้าที่ค้นหา");

                if (filter.Limit_Page == 0 && filter.Start_Page == 0)
                {
                    var productLists = new ProductListModel
                    {
                        Product_List = ProductFilter,
                        Product_Total = ProductFilter.Length
                    };
                    return productLists;
                }

                var productList = new ProductListModel
                {
                    Product_List = ProductFilter.Skip((filter.Start_Page - 1) * filter.Limit_Page)
                   .Take(filter.Limit_Page).ToArray(),
                    Product_Total = ProductFilter.Length
                };

                return productList;
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }

        }

        private ProductModel[] ProductOptionFilter(ProductModel[] products, string textSearch , string typeSearch)
        {
            if (!string.IsNullOrEmpty(textSearch) && !string.IsNullOrEmpty(typeSearch))
            {
                IEnumerable<ProductModel> Products = new ProductModel[] { };

                string SearchText = textSearch.ToUpper().Trim();
                string SearchType = typeSearch;
                switch (SearchType)
                {
                    case "Expired":
                        DateTime expiredRate = DateTime.Now.AddDays(Int32.Parse(textSearch));
                        Products = from it in products
                                   where it.Expired < expiredRate
                                   select it;
                        products = Products.ToArray();
                        break;

                    case "Amount_Product":
                        Products = from it in products
                                   where it.Amount_Product <= Int32.Parse(SearchText)
                                   select it;
                        products = Products.ToArray();
                        break;

                    default:
                        Products = from it in products
                                   where it.GetType().GetProperty(typeSearch)
                                         .GetValue(it).ToString().ToUpper().Trim()
                                         .Contains(SearchText)
                                   select it;
                        products = Products.ToArray();
                        break;
                }
            }
            return products;
        }

        public void UpdateProductById(ProductModel model, int id)
        {
            try
            {
                var product = this.db.Products.FirstOrDefault(it => it.Id.Equals(id));

                if (product == null) throw new Exception("ไม่พบสินค้าตามรหัส");

                product.Barcode = model.Barcode;
                product.Barcode_Custom = model.Barcode_Custom;
                product.Name = model.Name;
                product.Description = model.Description;
                product.Price = model.Price;
                product.Expired = model.Expired;
                product.Cost_Product = model.Cost_Product;
                product.ProductCategoryId = model.ProductCategoryId;
                product.Amount_Product = model.Amount_Product;
                product.Image_Url = model.Image_Url;
                product.Type = model.Type;
                product.Status = model.Status;
                product.Image_Byte = model.Image_Byte;
                product.Image_Type = model.Image_Type;

                this.onCoverBase64toImage(product, model.Image_Url);

                this.db.Products.Update(product);
                this.db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }

        }

        public void AdjustProductById(int count, int id, bool cancelBill)
        {
            try
            {
                var product = this.db.Products.FirstOrDefault(it => it.Id.Equals(id));

                if (product == null) throw new Exception("ไม่พบสินค้าตามรหัส");

                if (cancelBill)
                {
                    product.Amount_Product = (product.Amount_Product + count);
                }
                else
                {
                    product.Amount_Product = (product.Amount_Product - count);
                }

                this.db.Products.Update(product);
                this.db.SaveChanges();

                if (product.Amount_Product < count) throw new Exception("สินค้าในคลังติดลบ โปรดตรวจสอบ");
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }

        public void UpdateProductInStock(ProductAdjustModel model, int id)
        {
            try
            {
                var product = this.GetProductById(id);
                
                product.Amount_Product = model.amount_Product;

                this.db.Products.Update(product);
                this.db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            } 
        }

        private void onCoverBase64toImage(ProductModel product, string Image_Url)
        {
            /* ตรวจสอบว่ามีการส่งค่า image มารึป่าว แล้วทำการแยก type กับ image data ออกจากกัน เพื่อจะบันทึกข้อมูลลง db
             * frontend จะส่งข้อมูล image เป็น string โดยบรรจุ "type,image" มาให้     */

            if (!string.IsNullOrEmpty(Image_Url))
            {
                string[] image = Image_Url.Split(",");
                if (image.Length == 2)
                {
                    if (image[0].IndexOf("image") >= 0)
                    {
                        product.Image_Type = image[0];
                        product.Image_Byte = Convert.FromBase64String(image[1]);
                    }
                }
            }
        }
    }
}
