﻿using PPos.Interfaces;
using PPos.Models;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using PPos.Data;

namespace PPos.Services
{
    public class MemberServices : IMemberServices
    {
        private PosContext db;

        public MemberServices(PosContext _context)
        {
            db = _context;
        }

        public MemberProfileModel GetProfileMember(MemberModel member)
        {
            var UserLogin = member;
            if (UserLogin == null) return null;
            return new MemberProfileModel
            {
                Id = UserLogin.Id,
                Username = UserLogin.Username,
                Fristname = UserLogin.Fristname,
                Lastname = UserLogin.Lastname,
                Image_Byte = UserLogin.Image_Byte,
                Image_Type = UserLogin.Image_Type,
                Image_Url = UserLogin.Image_Url,
                Role = UserLogin.Role,
                Created = UserLogin.Created,
                Updated = UserLogin.Updated,
                Status = UserLogin.Status
            };
        }

        public void UpdateProfileMember(MemberProfileModel member, string username)
        {
            try
            {
                var MemberProfile = db.Members.SingleOrDefault(it => it.Username.Equals(username));

                if (MemberProfile == null) throw new Exception("not found username");

                UpdateMember(MemberProfile, member);
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }

        public void UpdateMember(MemberProfileModel member, int id)
        {
            try
            {
                var MemberUpdate = db.Members.SingleOrDefault(it => it.Id.Equals(id));
                if (MemberUpdate == null) throw new Exception("not found username");

                UpdateMember(MemberUpdate, member);
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }

        public void UpdateMember(MemberModel MemberProfile, MemberProfileModel member)
        {
            try
            {
                MemberProfile.Username = member.Username;
                MemberProfile.Fristname = member.Fristname;
                MemberProfile.Lastname = member.Lastname;
                MemberProfile.Status = member.Status;
                MemberProfile.Updated = DateTime.Now;
                MemberProfile.Role = member.Role;
                db.Members.Update(MemberProfile);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }

        }

        public void ChangePassword(PasswordChangeModel password, string username)
        {
            try
            {
                var MemberProfile = db.Members.SingleOrDefault(it => it.Username.Equals(username));
                if (MemberProfile == null) throw new Exception("not found username");
                if (!PasswordHash.VerifyPassword(password.Old_Password, MemberProfile.Password)) throw new Exception("old_password is valid");

                MemberProfile.Password = PasswordHash.HashPassword(password.New_Password);
                MemberProfile.Updated = DateTime.Now;

                db.Members.Update(MemberProfile);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }

        public MemberListModel GetAllMember()
        {
            var MemberList = new MemberListModel();

            var MemberAll = db.Members.Select(it => new MemberProfileModel
            {
                Id = it.Id,
                Username = it.Username,
                Fristname = it.Fristname,
                Lastname = it.Lastname,
                Role = it.Role,
                Status = it.Status,
                Updated = it.Updated,
                Created = it.Created
            }).ToArray();

            MemberList = new MemberListModel
            {
                Members = MemberAll,
                Total_Member = MemberAll.Length
            };

            return MemberList;
        }

        public MemberProfileModel GetMemberById(int id)
        {
            try
            {
                var member = this.db.Members.SingleOrDefault(it => it.Id.Equals(id));
                if (member == null) throw new Exception("not fornd member");

                return new MemberProfileModel
                {
                    Id = member.Id,
                    Fristname = member.Fristname,
                    Lastname = member.Lastname,
                    Username = member.Username,
                    Role = member.Role,
                    Updated = member.Updated,
                    Created = member.Created,
                    Status = member.Status,
                };
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }
    }
}
