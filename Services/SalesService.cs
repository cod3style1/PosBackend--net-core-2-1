﻿using PPos.Interfaces;
using PPos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Services
{
    public class SalesService : ISalesService
    {
        private PosContext db;
        public SalesService(PosContext _context)
        {
            db = _context;
        }

        public SalesModel AddSales(SalesModel model, string username)
        {
            try
            {
                int userId = this.db.Members.SingleOrDefault(it => it.Username.Equals(username)).Id;

                int totalPrice = this.getTotalPrice(model);

                int totalCostProduct =Convert.ToInt32(this.getTotalCostProduct(model));

                DateTime now = DateTime.Now;
                SalesModel sales = new SalesModel
                {
                    Sales_List = model.Sales_List,
                    Sales_Time = now,
                    Status = true,
                    MemberId = userId,
                    Total_Price = totalPrice,
                    Payment = model.Payment,
                    CostProductBill = totalCostProduct
                };

                if (sales.Total_Price > model.Payment) throw new Exception("insufficient amount");

                this.db.Sales.Add(sales);
                this.db.SaveChanges();
                return sales;
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }

        private int getTotalPrice(SalesModel model)
        {
            int totalPrice = 0;
            int priceProduct = 0;

            foreach (SalesOrderModel item in model.Sales_List)
            {
                priceProduct = this.getProductPrice(item.ProductId);

                totalPrice += (item.Sales_Count * priceProduct);
            }

            return totalPrice;
        }

        private decimal getTotalCostProduct(SalesModel model)
        {
            decimal totalCostProduct = 0;
            decimal costProduct = 0;
            foreach (SalesOrderModel item in model.Sales_List)
            {
                costProduct = this.getCostProduct(item.ProductId);

                totalCostProduct += (item.Sales_Count * costProduct);
            }

            return totalCostProduct;
        }

        private int getProductPrice(int id)
        {
            var product = this.db.Products.FirstOrDefault(it => it.Id.Equals(id));

            if (product == null) throw new Exception("ไม่พบสินค้าตามรหัส"); 

            return product.Price;
        }

        private Decimal getCostProduct(int id)
        {
            var product = this.db.Products.FirstOrDefault(it => it.Id.Equals(id));

            if (product == null) throw new Exception("ไม่พบสินค้าตามรหัส");

            return product.Cost_Product;
        }

        public SalesListModel GetAllSales(SalesOptionFilter filter)
        {
            try
            {
                SalesModel[] selesFilter = new SalesModel[] { };

                var sales = this.db.Sales.Select(it => new SalesModel
                {
                    Id = it.Id,
                    Sales_List = it.Sales_List,
                    Total_Price = it.Total_Price,
                    Sales_Time = it.Sales_Time,
                    MemberId = it.MemberId,
                    Status = it.Status,
                    Payment = it.Payment,
                    CostProductBill = it.CostProductBill,
                    CancelSales_Time = it.CancelSales_Time
                }).ToArray();

                if (sales == null) throw new Exception("not found sales");

                selesFilter = this.SalesOptionFilter(sales, filter.Search_DefaultText, filter.Search_DefaultType);

                selesFilter = this.SalesOptionFilter(selesFilter, filter.Search_Text, filter.Search_Type);

                var salesList = new SalesListModel
                {
                    Sales_List = selesFilter.Skip((filter.Start_Page - 1) * filter.Limit_Page)
                                 .Take(filter.Limit_Page).ToArray(),
                    Sales_Total = selesFilter.Length
                };

                if (salesList.Sales_List.Length == 0) throw new Exception("not found sales filter");

                return salesList;
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }

        private SalesModel[] SalesOptionFilter(SalesModel[] sales, string searchText, string searchType)
        {
            if (!string.IsNullOrEmpty(searchText) && !string.IsNullOrEmpty(searchType))
            {
                IEnumerable<SalesModel> Sales = new SalesModel[] { };
                string SearchText = searchText.ToUpper();
                switch (searchType)
                {
                    case "TotalPrice":
                        int price = Int32.Parse(searchText);
                        Sales = from it in sales
                                where it.Total_Price <= price
                                select it;
                        break;
                    case "SalesNow": // Default search ค้นหาใบเสร็จของเดือนปัจจุบัน
                        DateTime date = DateTime.Now;
                        Sales = from it in sales
                                where it.Sales_Time.Month == date.Month && it.Sales_Time.Year == date.Year
                                select it;
                        break;
                    case "SalesTime":

                        var dateSearch = searchText.Split("&");
                        DateTime toDate = DateTime.Parse(dateSearch[0]);
                        DateTime fromDate = DateTime.Parse(dateSearch[1]);

                        Sales = from it in sales
                                where it.Sales_Time <= fromDate && it.Sales_Time >=  toDate
                                select it;                        
                        break;
                    default:
                        Sales = from it in sales
                                where it.GetType().GetProperty(searchType)
                                        .GetValue(it).ToString().ToUpper()
                                        .Contains(searchText)
                                select it;
                        break;
                }
               return sales = Sales.ToArray();
            }
            return sales;
        }

        public SalesModel GetSalesById(int id)
        {
            try
            {
                var salesItem = db.Sales.Select(it => new SalesModel
                {
                    Id = it.Id,
                    Sales_List = it.Sales_List.Select( order => new SalesOrderModel {
                           Id = order.Id,
                           Product = order.Product,
                           ProductId = order.ProductId,
                           Sales_Count = order.Sales_Count,
                           Sales_Time = order.Sales_Time,
                    }).ToList(),
                    Sales_Time = it.Sales_Time,
                    Total_Price = it.Total_Price,
                    CancelSales_Time = it.CancelSales_Time,
                    Payment = it.Payment,
                    MemberId = it.MemberId,
                    Status = it.Status,
                    CostProductBill = it.CostProductBill,
                    MemberCancelBill = it.MemberCancelBill,
                    Member =it.Member
                }).FirstOrDefault(it => it.Id.Equals(id));

                if (salesItem == null) throw new Exception("not found sales id");

                return salesItem;
            }
            catch (Exception ex) 
            {
                throw ex.GetErrorExceptoin();
            }
        }

        public void ChangeStatusSalesBill(int id, string username)
        {
            try
            {
                var userCancel = this.db.Members.SingleOrDefault(it => it.Username.Equals(username)).Username;
                var sales = this.db.Sales.FirstOrDefault(it => it.Id.Equals(id));

                if (sales == null) throw new Exception("not found sales id");

                sales.MemberCancelBill = userCancel;
                sales.CancelSales_Time = DateTime.Now;
                sales.Status = false;

                this.db.Sales.Update(sales);
                this.db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }
    }
}
