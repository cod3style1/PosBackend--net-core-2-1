﻿using PPos.Interfaces;
using PPos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PPos.Data;

namespace PPos.Services
{
    public class AccountServices : IAccountServices
    {
        private PosContext db;
        public AccountServices(PosContext _context)
        {
            db = _context;
        }

        public void Register(RegisterModel _register)
        {
            try
            {
                db.Add(new MemberModel
                {
                    Username = _register.Username,
                    Fristname = _register.Fristname,
                    Lastname = _register.Lastname,
                    Password = _register.Password,
                    Role = AccountRole.admin,
                    Created = DateTime.Now,
                    Updated = DateTime.Now,
                    Image_Url = _register.Image_Url,
                    Image_Byte = _register.Image_Byte,
                    Image_Type = _register.Image_Type,
                    Status = true
                });
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }

        public bool Login(LoginModel _login)
        {
            try
            {
                //เช็คว่า Email ที่ กรอกมา ตรงกับใน Model รึป่าว
                var member = this.db.Members.SingleOrDefault(it => it.Username.Equals(_login.Username));
                if (member == null) throw new Exception("username is wrong");

                //ตรวจ password ที่ส่งมา ว่าตรงกับ DB รึป่าว แล้ว return boolean ออกมา
                return PasswordHash.VerifyPassword(_login.Password, member.Password); // retune boolean
            }
            catch (Exception ex)
            {
                throw ex.GetErrorExceptoin();
            }
        }
    }
}
