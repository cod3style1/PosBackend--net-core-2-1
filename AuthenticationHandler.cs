﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using PPos.Interfaces;
using PPos.Models;
using PPos.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;

namespace PPos
{
    public class AuthenticationHandler : IMiddleware
    {
        private IAccessTokenServices accessTokenService;
        private PosContext context;

        public AuthenticationHandler(PosContext _context)
        {
            context = _context;
            accessTokenService = new JwtAccessTokenService(context);
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var token = context.Request.Headers["Authorization"];
            try
            {
                if (!string.IsNullOrEmpty(token))
                {
                    var accessToken = token.ToString().Replace("Bearer", "").Trim();
                    var UserVerify = accessTokenService.VerrifyAccessToken(accessToken);

                    if (UserVerify != null)
                    {
                        var identity = new GenericIdentity(UserVerify.Username);
                        context.User = new GenericPrincipal(identity, new string[] { UserVerify.Role.ToString() });
                    }
                }
                await next(context);
            }
            catch (Exception exception)
            {
                context.Response.StatusCode = StatusCodes.Status400BadRequest;
                context.Response.Headers.Add("content-type", "application/json");
                await context.Response.WriteAsync(JsonConvert.SerializeObject(exception.GetErrorExceptoin().Message));
            }
        }

    }

    public static class BuildMiddleWare
    {
        public static IApplicationBuilder UseAuthenticationHander(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AuthenticationHandler>();
        }
    }
}
