﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PPos.Models;

namespace PPos.Interfaces
{
    interface IOrderProductService
    {
        void InsertOrderProduct(OrderProductListModel model, string username);

        // แสดงข้อมูลการสั่งซื้อแบบรวมรายการ
        OrderProductListModel[] GetOrderProducts(OrderOptionFilter filter);

        // แสดงข้อมูลการสั่งซื้อด้วย ID
        OrderProductListModel GetOrderProductById(int id);

        // อับเดจรายการสั่งซื้อ
        void UpdateOrderById(OrderProductListModel model , int id, string username);

        // เพิ่มสินค้าเข้าในระบบตามบิล
        void AdjustOrderProductById(int id, string username);

        // ยกเลิกการสั่งซื้อสินค้า
        void CancelOrderProductById(int id);
    }
}
