﻿using PPos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Interfaces
{
    interface IAccessTokenServices
    {
        // สร้าง Token
        string GenerateAccessToken(string _username, int minute = 60);

        // ตรวจสอบ Token
        MemberModel VerrifyAccessToken(string accessToken);
    }
}
