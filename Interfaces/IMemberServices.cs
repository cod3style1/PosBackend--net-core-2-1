﻿using PPos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Interfaces
{
    interface IMemberServices
    {
        // แสดงข้อมุลผู้ที่ล็อคอินคนปัจจุบัน
        MemberProfileModel GetProfileMember(MemberModel member);

        // อับเดจข้อมุลผู้ที่ล็อคอินคนปัจจุบัน
        void UpdateProfileMember(MemberProfileModel member, string username);

        // อับเดจข้อมุลผุ้ใช้ในระบบด้วย ID
        void UpdateMember(MemberProfileModel member, int id);

        // เปรียนรหัสผ่านผู้ใช้ในระบบ
        void ChangePassword(PasswordChangeModel password, string username);

        // แสดงข้อมูลผู้ใช้รายบุลคล
        MemberProfileModel GetMemberById(int id);

        // แสดงข้อมูลผู้ใช้ทั้งหมด
        MemberListModel GetAllMember();
    }
}
