﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PPos.Models;

namespace PPos.Interfaces
{
    interface IAdjustProductService
    {
        // ปรับสต็อลสินค้าเมื่อมีการขายของ
        void AdjustStockProduct(SalesModel model);

        // ปรับสต็อคสินค้าเมื่อมีการยกเลิกบิล
        void CancelBill(SalesModel model);
    }
}
