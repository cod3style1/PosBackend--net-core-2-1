﻿using PPos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Interfaces
{
    interface ISalesService
    {
        // เพิ่มรายการขาย
        SalesModel AddSales(SalesModel model,string username);

        // แสดงรายการขายทั้งหมด
        SalesListModel GetAllSales(SalesOptionFilter filter);

        // เปลียนสถานะของบิลเมื่อมีการยกเลิกบิล
        void ChangeStatusSalesBill(int id, string username);

        // แสดงสินค้าด้วย ID
        SalesModel GetSalesById(int id);
    }
}
