﻿using PPos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Interfaces
{
    interface ICategoryService
    {
        // เพิ่มหมวดหมู่สินค้า
        void AddCategory(CategoryProductModel model);

        // แสดงหมวดหมุ่สินค้าทั้งหมด
        CategoryListModel GetCategory();

        // แสดงหมวดหมุ่สินค้าด้วย ID
        CategoryProductModel GetCategoryById(int id);

        // อับเดจหมวดหมู่สินค้า
        void UpdateCategoryById(CategoryProductModel model, int id);
    }
}
