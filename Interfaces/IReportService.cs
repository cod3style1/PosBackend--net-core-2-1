﻿using PPos.Models;
using PPos.Models.ReportModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Services
{
    interface IReportService
    {
        // แสดงรายการยอดขายทั้งหมด
        ReportSalesListModel GetSalesReport(ReportOptionFilter filter); 

        // แสดงยอดขายของสินคื้าแต่ละชนิด
        ReportSalesProductListModel GetProductSalesReportById(ReportOptionFilter filter, int _id);

        // เปรียบเทียบยอดขายของสินค้า 2 ชนิดขึ้นไป ** refactor code **
        ReportProductCompareModel GetCompareProductSalesReport(ReportOptionFilter filter);

        ReportBestSalerList  GetProductBestSales(ReportOptionFilter filter);

        CostAndProfitList GetCostAndProfit(ReportOptionFilter filter);

    }
}
