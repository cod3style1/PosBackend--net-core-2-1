﻿using PPos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Interfaces
{
    interface IProductService
    {
        // เพิ่มสินค้าเข้าในระบบ
        void AddProduct(ProductModel model);

        // แสดงข้อมูลสินค้ทั้งหมด
        ProductListModel GetProducts(ProductOptionFilter filter);

        // แสดงข้อมูลสินค้าด้วย ID
        ProductModel GetProductById(int id);

        // อัยเดจสินค้า
        void UpdateProductById(ProductModel model, int id);

        // ตัดหรือเพิ่มจำนวนสินค้าในระบบ
        void AdjustProductById(int count, int id, bool cancelBill);

        // ปรับยอดสินค้าคงคลัง
        void UpdateProductInStock(ProductAdjustModel model, int id);
    }
}
