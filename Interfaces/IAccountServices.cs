﻿using PPos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Interfaces
{
    interface IAccountServices
    {
        // เข้าสู้ระบบ
        bool Login(LoginModel _login);

        // ลงทะเบียนสมาชิก
        void Register(RegisterModel _register);
    }
}