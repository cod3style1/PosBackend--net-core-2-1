﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PPos.Data;
using PPos.Interfaces;
using PPos.Models;
using PPos.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PPos.Controllers
{
    [Route("api/[controller]/[action]")]
    public class AccountController : Controller
    {
        private PosContext context;
        private IAccountServices account;
        private IAccessTokenServices accessTokenService;

        public AccountController(PosContext _context)
        {
            context = _context;
            account = new AccountServices(context);
            accessTokenService = new JwtAccessTokenService(context);
        }

        [HttpPost]
        [ActionName("login")]
        public IActionResult Login([FromBody]LoginModel _login)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (account.Login(_login))
                    {
                        return Ok(new AccessTokenStringModel
                        {
                            AccessToken = accessTokenService.GenerateAccessToken(_login.Username)
                        });
                    }
                    throw new Exception("password is worng");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Exception", ex.Message);
                }
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }

        [HttpPost]
        [ActionName("register")]
        public IActionResult Register([FromBody]RegisterModel _register)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _register.Password = PasswordHash.HashPassword(_register.Password);
                    this.account.Register(_register);
                    return Ok();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Exception", ex.Message);
                }
            }
            return BadRequest(ModelState.getErrorModelStage());
        }
    }
}
