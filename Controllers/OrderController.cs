﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PPos.Interfaces;
using PPos.Models;
using PPos.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PPos.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class OrderController : Controller
    {
        private PosContext context;
        private IOrderProductService orderProductService;
        public OrderController(PosContext _context)
        {
            context = _context;
            orderProductService = new OrderProductService(context);
        }
        [Authorize(Roles = "admin")]
        [HttpPost]
        [ActionName("insert-order")]
        public IActionResult InsertOrderProduct([FromBody] OrderProductListModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    this.orderProductService.InsertOrderProduct(model, User.Identity.Name);
                    return Ok();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Exception", ex.Message);
                }
            }

            return BadRequest(ModelState.GetErrorModelStage());
        }

        [HttpGet]
        [ActionName("orders")]
        public object GetAllOrder(OrderOptionFilter filter)
        {
            try
            {
                return this.orderProductService.GetOrderProducts(filter);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Exception", ex.Message);
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }

        [HttpGet]
        [ActionName("orders/{id}")]
        public object GetOrderById(int id)
        {
            try
            {
                return this.orderProductService.GetOrderProductById(id);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Exception", ex.Message);
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        [ActionName("cancel-order/{id}")]
        public IActionResult CancelOrderById(int id)
        {
            try
            {
                this.orderProductService.CancelOrderProductById(id);
                return Ok();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Exception", ex.Message);
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        [ActionName("adjust-order/{id}")]
        public IActionResult AdjustOrderById(int id)
        {
            try
            {
                this.orderProductService.AdjustOrderProductById(id, User.Identity.Name);
                return Ok();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Exception", ex.Message);
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        [ActionName("update-order/{id}")]
        public IActionResult UpdateOrderById([FromBody] OrderProductListModel model, int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    this.orderProductService.UpdateOrderById(model, id, User.Identity.Name);
                    return Ok();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Exception", ex.Message);
                }
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }
    }
}
