﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PPos.Interfaces;
using PPos.Models;
using PPos.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PPos.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class ProductController : Controller
    {
        private PosContext context;
        private IProductService productService;

        public ProductController(PosContext _context)
        {
            context = _context;
            productService = new ProductService(context);
        }
        

        [HttpPost]
        [ActionName("add-product")]
        public IActionResult AddProduct([FromBody]ProductModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    this.productService.AddProduct(model);
                    return Ok();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Exception", ex.Message);
                }
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }


        [HttpGet]
        [ActionName("products")]
        public object GetProducts(ProductOptionFilter filter)
        {
            try
            {
                return this.productService.GetProducts(filter);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Exception", ex.Message);
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }

        [HttpGet]
        [ActionName("product/{id}")]
        public object GetProductById(int id)
        {
            try
            {
                var product = this.productService.GetProductById(id);
                return product;
            }
            catch (Exception ex)
            {

               ModelState.AddModelError("Exception", ex.Message);
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        [ActionName("update-product/{id}")]
        public IActionResult UpdateProductById([FromBody]ProductModel model, int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    this.productService.UpdateProductById(model, id);
                    return Ok();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Exception", ex.Message);
                }
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }

        [AllowAnonymous]
        [HttpGet]
        [ActionName("insert-test")]
        public void InsertProductTest()
        {
            Random random = new Random();
            string[] nameProduct = new string[] {
                "Jaybird Freedom 2 Wireless", "Peppi", "Caca cola", "Unif", "Singha", "Cp food", "Change", "Rosa", "Sapi", "Losan",
                "Asus", "Hp", "Critra", "Acer", "Fiio", "Oppo", "Yenti", "Samsung", "nike", "lomad",
                "Windows", "Sesies sd", "Toshiba", "Baby mind", "Luck", "Tiwgo", "Gambo", "Rosa", "Sapi", "Losan",
                "Tesgo", "Tipco", "Ora cola", "Preze", "Motorosa", "Loset", "Bgvp Ds1", "Kz Zst", "Kz Zs5", "Kz Ba 10",
                "Tfz Excusive 1", "Tfz Excusive 5", "Tfz Excusive 2", "Jbt", "Sony Go", "Pk 1", "Fiio Btr 1", "Fiio Btr3", "Fiio Fh5", "Sony Wn32",
                "Sony Walkman", "Camfire kW", "Senerser s1", "Astell&Kern Layla II", "Audeze LCDi4", "Astell&Kern Layla II", "Noble Audio Savant Universal Ⅱ", "OSTRY KC06A", " Kinera BD005 V.2", "Shure SE215 Special Edition",
                "Kinera BD005 V.2 ", "Shure KSE1500", "Bose SoundSport Wireless", "Shure KSE1500", "Visang R02", " JBL Free", "Shure SE215", " i.Tech FreeStereo Twins 2", "JBL E25BT ", "Hifiman Re400",
                "Beats Powerbeats 3 Wireless", "Jaybird Run", "B&O BeoPlay H5", "Sony NW-WS413 ", "Bose SoundSport Wireless", "iBasso IT01 ", "Shure SE315", "Astell&Kern Layla II", "B&O BeoPlay H5", "Zero Audio DWX10",
                "Aroma Witch Girl 12", "Campfire Audio Orion Cerakote Edition", "Sony XBA-N3AP", "Sony WF-1000X", "Sony NW-WS623", "Jaybird Run", "Creative Outlier Sports", "Aroma Witch Girl Pro", "Audio Technica ATH-LS400iS", "Sony WF-SP700N",
                "Shure SE112M", "Kinera H3", "Zero Audio BX700", "Audio Technica ATH E70", "Sony MDR-XB510AS", "Visang R04", "Change", "Rosa", "Sapi", "Losan",
                "Bose SoundSport Free", "Noble Audio Kaiser Encore", "AKG N20NC", "Ostry KC06", "Marshall Mode", "Havit G1", "Audio Technica ATH-LS50iS ", "Sony MDR XB50BS Bluetooth", "Sony NW-WS413 (No BT)", "Visang R04",
                "RHA MA600i", "i.Tech FreeStereo Twins 2", "Kinera H3", "JBL Under Armour", "Sony WI-SP600N", "Sennheiser Presence ", "RHA CL750", "B&O BeoPlay H3 Android", "MoonDrop Kanas", "JBL Reflect Fit",
                "Custom JH Audio Ambient FR","Custom JH Audio","Custom Noble Audio","Custom Eartech Triple","Sony NW-WM1Z","iBasso DX120 ","Fiio X5 Gen 3","Opus #1 Premium","Astell&Kern A&Altima ","iBasso DX120","Opus #1S  ","HiFiMAN Megamini","Sennheiser GSP 550  ","Audeze Mobius","Audeze Mobius"

            };
            string[] sizeProducr = new string[] { "W002", "XL", "S", "M", "200ml", "250ml", "300ml", "1.25l", "1.5l", "130g", "110g" };
            Array typeValues = Enum.GetValues(typeof(ProductType));
            Array categoryValue = Enum.GetValues(typeof(ProductCategory));
            int rang = 5 * 365;
            int cost = random.Next(20, 300);

            for (int i = 0; i <= 100; i++)
            {
                ProductModel model = new ProductModel
                {
                    Name = nameProduct[random.Next(0, nameProduct.Length)] + " " + sizeProducr[random.Next(0, sizeProducr.Length)],
                    Barcode = "885001" + random.Next(1000000, 9999999).ToString(),
                    Barcode_Custom = random.Next(0, 999).ToString(),
                    Description = "More..",
                    Amount_Product = random.Next(0, 200),
                    Price = cost + random.Next(10,50),
                    Cost_Product = cost,
                    Expired = DateTime.Now.AddDays(random.Next(rang)),
                    ProductCategoryId = (ProductCategory)categoryValue.GetValue(random.Next(1, categoryValue.Length)),
                    Status = true,
                    Type = (ProductType)typeValues.GetValue(random.Next(1, typeValues.Length))
                };
                this.AddProduct(model);
            }
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        [ActionName("update-productstock/{id}")]
        public IActionResult UpdateProductStock([FromBody] ProductAdjustModel model, int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    this.productService.UpdateProductInStock(model, id);
                    return Ok();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Exception", ex.Message);
                }
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }
    }
}
