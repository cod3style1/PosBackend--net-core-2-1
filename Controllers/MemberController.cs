﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PPos.Interfaces;
using PPos.Services;
using PPos.Models;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PPos.Controllers
{

    [Authorize]
    [Route("api/[controller]/[action]")]
    public class MemberController : Controller
    {
        private PosContext context;
        private IMemberServices MemberService;

        public MemberController(PosContext _context)
        {
            context = _context;
            MemberService = new MemberServices(context);
        }

        [HttpGet]
        [ActionName("data")]
        public MemberProfileModel GetMemberLogin()
        {
            return MemberService.GetProfileMember(context.Members.SingleOrDefault(it => it.Username.Equals(User.Identity.Name)));
        }

        [HttpPost]
        [ActionName("profile")]
        public IActionResult UpdateProfile([FromBody]MemberProfileModel member)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    MemberService.UpdateProfileMember(member, User.Identity.Name);
                    return Ok();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Exception", ex.Message);
                }
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        [ActionName("get-member/{id}")]
        public MemberProfileModel GetMember(int id)
        {
            try
            {
                return this.MemberService.GetMemberById(id);
                throw new Exception("not found member");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Exception", ex.Message);
                throw  new Exception(ex.Message);
            }
        }


        [HttpPost]
        [Authorize(Roles = "admin")]
        [ActionName("update-member/{id}")]
        public IActionResult UpdateMember([FromBody]MemberProfileModel member, int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    MemberService.UpdateMember(member, id);
                    return Ok();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Exception", ex.Message);
                }
                return BadRequest(ModelState.GetErrorModelStage());
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }

        [HttpPost]
        [ActionName("chang-password")]
        public IActionResult ChangePassword([FromBody]PasswordChangeModel password)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    MemberService.ChangePassword(password, User.Identity.Name);
                    return Ok();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Exception", ex.Message);
                }
                return BadRequest(ModelState.GetErrorModelStage());
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        [ActionName("all-member")]
        public object GetAllMember()
        {
            try
            {
                return MemberService.GetAllMember();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Exception", ex.Message);
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }

        [HttpDelete]
        [Authorize(Roles = "admin")]
        [ActionName("delete/{id}")]
        public IActionResult deleteAllMember(int id)
        {
            var memberall = this.context.Members.SingleOrDefault(it => it.Id.Equals(id));
            this.context.Members.Remove(memberall);
            this.context.SaveChanges();
            return Ok("delete success");
        }
    }
}
