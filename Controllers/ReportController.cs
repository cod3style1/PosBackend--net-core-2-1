﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PPos.Models;
using PPos.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PPos.Controllers
{
    [Route("api/[controller]/[action]")]
    public class ReportController : Controller
    {
        private PosContext context;
        private IReportService reportService;

        public ReportController(PosContext _context)
        {
            context = _context;
            reportService = new ReportService(context);
        }

        [HttpGet]
        [ActionName("sales")]
        public object GetAllSales(ReportOptionFilter filter)
        {
            try
            {
                return this.reportService.GetSalesReport(filter);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Exception", ex.Message);
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }

        [HttpGet]
        [ActionName("sales-product/{id}")]
        public object GetProductSalesById(ReportOptionFilter filter, int id)
        {
            try
            {
                return this.reportService.GetProductSalesReportById(filter, id);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Exception", ex.Message);
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }

        [HttpGet]
        [ActionName("compare-product")]
        public object GetProductSalesCompare(ReportOptionFilter filter)
        {
            try
            {
                return this.reportService.GetCompareProductSalesReport(filter);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Exception", ex.Message);
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }

        [HttpGet]
        [ActionName("best-sales")]
        public object GetProductBestSeles(ReportOptionFilter filter)
        {
            try
            {
                return this.reportService.GetProductBestSales(filter);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Exception", ex.Message);
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }

        [HttpGet]
        [ActionName("cost-profit")]
        public object GetCostAndProfit(ReportOptionFilter filter)
        {
            try
            {
                return this.reportService.GetCostAndProfit(filter);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Exception", ex.Message);
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }
    }
}
