﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using PPos.Interfaces;
using PPos.Models;
using PPos.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PPos.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class AdjustProductController : Controller
    {
        private PosContext context;
        private ISalesService salesService;
        private IAdjustProductService stockProductService;

        public AdjustProductController(PosContext _context)
        {
            context = _context;
            salesService = new SalesService(context);
            stockProductService = new AdjustProductService(context);
        }

        [HttpPost]
        [ActionName("adjust-product")]
        public IActionResult AdJustProductStock([FromBody]SalesModel model)
        {
            if (ModelState.IsValid)
            { 
                try
                {
                    var salesBill = this.salesService.AddSales(model, User.Identity.Name);
                    this.stockProductService.AdjustStockProduct(model);
                    return Ok( new { id = salesBill.Id });
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Exception", ex.Message);
                }
            }
            return null;
        }

        [HttpGet]        
        [ActionName("cancelbill-product/{id}")]
        public IActionResult CancelBillProduct(int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var salesBill = this.salesService.GetSalesById(id);
                    if (salesBill.Status == false) throw new Exception("ใบเสร็จนี้ ทำการยกเลิกบิลไปแล้ว");

                    this.stockProductService.CancelBill(salesBill);
                    this.salesService.ChangeStatusSalesBill(id, User.Identity.Name);
                    return Ok();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Exception", ex.Message);
                }
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }

        [AllowAnonymous]
        [HttpGet]
        [ActionName("pustproduct-test")]
        public  void PushProductTest()
        {
            Random random = new Random();

            int rang = 60;

            for (int i = 0; i <= 30; i++)
            {
                DateTime timeRandom = DateTime.Now.AddDays(random.Next(rang));

                SalesOrderModel[] order = new SalesOrderModel[] {
                new SalesOrderModel { ProductId = random.Next(1536, 1644), Sales_Count = random.Next(1, 3), Sales_Time = timeRandom },
                new SalesOrderModel { ProductId = random.Next(1536, 1644), Sales_Count = random.Next(1, 3), Sales_Time = timeRandom },
                new SalesOrderModel { ProductId = random.Next(1536, 1644), Sales_Count = random.Next(1, 3), Sales_Time = timeRandom }
            };

                SalesModel model = new SalesModel
                {
                    Sales_List = order.ToList(),
                    Sales_Time = timeRandom,
                    MemberId = 1,
                    Status = true,
                    Payment = 15000
                };
                this.AdJustProductStock(model);
            }
        }
    }
}
