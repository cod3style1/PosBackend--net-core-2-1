﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PPos.Interfaces;
using PPos.Models;
using PPos.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PPos.Controllers
{
    [Route("api/[controller]/[action]")]
    public class SalesController : Controller
    {
        private PosContext context;
        private ISalesService salesService;
        private IAdjustProductService stockProductService;

        public SalesController(PosContext _context)
        {
            context = _context;
            salesService = new SalesService(context);
            stockProductService = new AdjustProductService(context);
        }

        [HttpGet]
        [ActionName("all-sales")]
        public object GetAllSales(SalesOptionFilter filter)
        {
            try
            {
                return this.salesService.GetAllSales(filter);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Exception", ex.Message);
            }

            return BadRequest(ModelState.GetErrorModelStage());
        }

        [HttpGet]
        [ActionName("get-sales/{id}")]
        public object GetSalesById(int id)
        {
            try
            {
                return this.salesService.GetSalesById(id);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Exception", ex.Message);
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }
    }
}
