﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PPos.Interfaces;
using PPos.Models;
using PPos.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PPos.Controllers
{
    [Route("api/[controller]/[action]")]
    public class CategoryController : Controller
    {
        private PosContext context;
        private ICategoryService categoryService;

        public CategoryController(PosContext _context)
        {
            context = _context;
        }

        [HttpPost]
        [ActionName("add-category")]
        public IActionResult AddCategory([FromBody]CategoryProductModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    categoryService.AddCategory(model);
                    return Ok("add category success");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Exception", ex.Message);
                }
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }

        [HttpGet]
        [ActionName("categorys")]
        public object GetCategory()
        {
            try
            {
                return this.categoryService.GetCategory();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Exception", ex.Message);
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }

        [HttpGet]
        [ActionName("category/{id}")]
        public object GetCategoryById(int id)
        {
            try
            {
                return this.categoryService.GetCategoryById(id);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Exception", ex.Message);
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }

        [HttpPost]
        [ActionName("update-category/{id}")]
        public IActionResult UpdateCategoryById([FromBody]CategoryProductModel model, int id)
        {
            try
            {
                this.categoryService.UpdateCategoryById(model, id);
                return Ok();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Exception", ex.Message);
            }
            return BadRequest(ModelState.GetErrorModelStage());
        }

    }
}
