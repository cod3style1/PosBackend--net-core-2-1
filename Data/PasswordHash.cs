﻿using SimplePassword;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPos.Data
{
    public class PasswordHash
    {
        //hash password
        public static string HashPassword(string password)
        {
            var saltedpasswordHash = new SaltedPasswordHash(password, 21);
            return saltedpasswordHash.Hash + ":" + saltedpasswordHash.Salt;
        }

        //verify password
        public static bool VerifyPassword(string password, string passwordHash)
        {
            string[] passwordHashes = passwordHash.Split(":");
            if (passwordHashes.Length != 2) return false;

            var saltedpasswordHash = new SaltedPasswordHash(passwordHashes[0], passwordHashes[1]);
            return saltedpasswordHash.Verify(password);
        }
    }
}
